from django.apps import AppConfig


class WebsysprogConfig(AppConfig):
    name = 'websysprog'

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse, JsonResponse

def index(request):
	html = 'index.html'
	return render(request, html)

def cart(request):
	html = 'cart.html'
	return render(request,html)